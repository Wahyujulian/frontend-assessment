# React Quiz Assessment Frontend Developer

Pada assessment ini kamu harus membuat sebuah aplikasi untuk mencatat keperluan belanja (Shopping List) yang dimana aplikasi tersebut dapat melakukan beberapa hal seperti tambah, ubah dan hapus list belanja.

Aplikasi dibangun dengan menggunakan framework [ReactJS](https://react.dev/) dan memiliki user interface seperti berikut:

![list](assets/after-update.png)

## Ketentuan Pengerjaan

Sebelum mengerjakan assessment ini ada beberapa hal yang harus diperhatikan yaitu:

1. Tampilan dari aplikasi harus sama dengan contoh yang diberikan
2. Assessment ini harus dikerjakan pada file [jawaban.jsx](quiz/jawaban.jsx) yang ada di folder [quiz](quiz) yang telah disediakan
3. Jika ingin melakukan code splitting dengan menambahkan file baru sangat diperbolehkan, akan tetapi hanya boleh ditambahkan pada folder [quiz](quiz)
4. Baca To Do assessment dengan baik dan kerjakan sesuai dengan To Do yang disampaikan

## To Do

Berikut merupakan ketentuan & alur dari aplikasi yang harus dibangun:

### 1. List Kosong

- Pada saat table kosong maka menampilkan tampilan sebagai berikut:
- ![empty](assets/empty.png)

### 2. Tambah Data

- User dapat menambahkan shopping list dengan cara mengisi form yang ada disebelah kiri

![create](assets/create.png)

- Jika salah satu field Product/Qty tidak diisi pada saat menekan tombol submit maka menampilkan alert seperti berikut

![validation](assets/validation.png)

- Jika semua field telah diisi dan tombol submit ditekan maka data pada form akan disimpan dan muncul pada tabel
- berikan alert agar user mengetahui bahwa data telah berhasil disimpan

![create alert](assets/create-alert.png)

### 3. Ubah Data

- User dapat mengubah data yang ada pada tabel shopping list dengan cara menekan tombol <b>Edit</b> yang ada pada setiap baris pada tabel
- Data yang di edit akan langsung mengisi form yang ada disebelah kiri
- Tomboh <b>Submit</b> pada form akan berubah menjadi <b>Update</b>
- Sama seperti pada aksi tambah data, pada saat ubah data semua field harus terisi

![update](assets/update.png)

- Setelah selesai melakukan ubah data pada form maka gunakan tombol update untuk mengubah data pada tabel
- Berikan alert agar user mengetahui bahwa data telah berhasil diubah

![update-alert](assets/update-alert.png)

- Setelah melakukan ubah maka semua field pada form dikosongkan dan ubah kembali button Update menjadi Submit

![after-update](assets/after-update.png)

### 4. Hapus Data

- User dapat menghapus data yang ada pada tabel shopping list dengan cara menekan tombol <b>Delete</b> yang ada pada setiap baris pada tabel
- Tampilkan konfirmasi
- Jika tombol OK ditekan maka hapus data dari tabel

![delete-confirm](assets/delete-confirm.png)

- Berikan alert agar user mengetahui bahwa data telah berhasil dihapus

![delete-alert](assets/delete-alert.png)
