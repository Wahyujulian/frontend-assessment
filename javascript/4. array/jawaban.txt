Jawaban:

1. Array adalah struktur data untuk menyimpan nilai-nilai atau data-data dengan menggunakan indeks sebagai identfikasi posisi nilai
2. 5 built in method array pada javascript yaitu : - push() => digunakan untuk menambahkan satu atau lebih elemen baru ke akhir array
                                                   - pop() => digunakan untuk menghapus elemen terakhir pada array
                                                   - splice() => digunakan untuk menambahkan, menghapus, atau mengganti elemen dalam array dengan mengubah isi aslinya 
                                                   - join() => digunakan untuk menggabungkan semua elemen array menjadi satu string 
                                                   - concat() => digunakan untuk menggabungkan dua atau lebih array menjadi satu array baru
