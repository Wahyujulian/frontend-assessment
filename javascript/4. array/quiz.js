/**
 * Berikan jawabanmu dibawah ini!
 */

 let vehicle1 = {
    name: "Sepeda",
    color: "Hijau",
    price: 700000,
    start: function(){
        return this.name + " sudah berangkat dari tadi";
    },
    stop: function(){
        return this.name + "sudah berhenti dari tadi";
    }
};

let vehicle2 = {
    name: "Mobil",
    color: "Merah",
    price: 80000,
    start: function(){
        return this.name + " sudah berangkat dari tadi";
    },
    stop: function(){
        return this.name + "sudah berhenti dari tadi";
    }
};

let vehicle3 = {
    name: "Motor",
    color: "Putih",
    price: 500000,
    start: function(){
        return this.name + " sudah berangkat dari tadi";
    },
    stop: function(){
        return this.name + "sudah berhenti dari tadi";
    }
};

let vehicle4 = {
    name: "Bus",
    color: "Biru",
    price: 45000,
    start: function(){
        return this.name + " sudah berangkat dari tadi";
    },
    stop: function(){
        return this.name + "sudah berhenti dari tadi";
    }
};

let vehicle5 = {
    name: "Pesawat",
    color: "Ungu",
    price: 550000,
    start: function(){
        return this.name + " sudah berangkat dari tadi";
    },
    stop: function(){
        return this.name + "sudah berhenti dari tadi";
    }
};

let dataVehicle = [vehicle1, vehicle2, vehicle3, vehicle4, vehicle5];

console.log(dataVehicle);
