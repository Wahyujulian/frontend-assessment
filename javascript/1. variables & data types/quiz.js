/**
 * Berikan jawabanmu dibawah ini!
 */

//variable dengan 'var'

var nama = "Wahyu Julian"; //tipe data string

var usia = 21; //tipe data number

var awal = true; //tipe data boolean

var dataArray = [1, 2, 3]; //tipe data array

var user = {
    nama : "Wahyu Julian",
    usia : 21
}; //tipe data object

//================================================
//variable dengan 'let'

let nama = "Wahyu Julian"; //tipe data string

let usia = 21; //tipe data number

let status = false; //tipe data boolean

let dataArray = [1, 2, 3]; //tipe data array

let user = {
    nama : "Wahyu Julian",
    usia : 21
}; //tipe data object

//===============================================
//variable dengan 'const'

const asal = "Singaraja"; //tipe data string

const tahunLahir = 2002; //tipe data number

const statusUjian = true; //tipe data boolean

const anggotaKelompok = ["Kadek", "Wayan", "Gede"]; //tipe data array

const kendaraan = {
    jenis : "Mobil",
    totalRoda : 4,
    merk : "APV"
}; //tipe data object