# Javascript Variables & Data Types

## Soal

1. Apa itu variables?
2. Apa itu data types?
3. Apa saja cara untuk mendeklarasikan variable pada javascript?
4. Apa saja data types yang ada pada javascript?

## Quiz

Buatlah contoh deklarasi variables dengan semua cara yang ada, lalu pada setiap cara pendeklarasian variables harus berisikan contoh yang memuat semua tipe data
