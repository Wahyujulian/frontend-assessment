/**
 * Berikan jawabanmu dibawah ini!
 */

    let vehicle = {
        name: "Sepeda",
        color: "Hijau",
        price: 700000,
        start: function(){
            return this.name + " sudah berangkat dari tadi";
        },
        stop: function(){
            return this.name + "sudah berhenti dari tadi";
        }
    };

    //Memanggil methods dalam objek
    console.log(vehicle.start());
    console.log(vehicle.stop());

    //Mengakses properties dalam objek
    console.log(vehicle.name);
    console.log(vehicle.color);
    console.log(vehicle.price);
