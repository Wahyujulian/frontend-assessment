# Javascript Object

## Soal

1. Apa itu object pada javascript?

## Quiz

Buatlah sebuah object yang bernama vehicle dengan ketentuan sebagai berikut:

- properties:
  - name
  - color
  - price
- methods:
  - start
  - stop
