/**
 * Berikan jawabanmu dibawah ini!
 */

let nilai = 87;

if(nilai >= 85){
    console.log("Selamat, kamu mendapatkan grade A");
} else if(nilai >= 75){
    console.log("Selamat, kamu mendapatkan grade B");
}else{
    console.log("Maaf kamu harus remidi");
}

switch (nilai){
    case 85:
        console.log("Selamat, kamu mendapatkan grade A");
        break;
    case 75:
        console.log("Selamat, kamu mendapatkan grade B");
        break;
    default:
        console.log("Maaf kamu harus remidi");
}

let hasil = (nilai >= 75) ? "Selamat kamu lulus" : "Maaf kamu harus remidi";
console.log(hasil);