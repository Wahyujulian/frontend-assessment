Jawaban:

1. Tipe-tipe kondisional/percabangan yang ada pada javascript adalah 
    - if => digunakan jika kondisi bernilai 'true'
    - if ... else => jika memiliki 2 kondisi yaitu bernilai 'true' dan 'false'
    - if... else if... else => jika memiliki lebih dari 2 kondisi
    - Ternary Operator ('? :') => kondisi nya sama seperti if...else, mengevaluasi suatu kondisi dan menghasilkan dua nilai yang berbeda berdasarkan kondisi tersebut
    - Switch...case => kondisinya sama seperti if...else if...else, mengevaluasi satu variable pada beberapa nilai yang berbeda